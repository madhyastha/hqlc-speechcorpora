#!/bin/bash

# A walkthrough of SVB ASR system

. cmd.sh
. path.sh
set -e # exit on error

stage=$1

if [ -z $1 ]; then
	echo "Specify stage";
	exit;
fi
#############################################
# Stage 1
#############################################
if [ $stage = 1 ]; then
	# in the following directory: data/local/train/
	# 1)copy: reco2file_and_channel wav.scp
	# 2)create:  uttr.id    word.id
	# 3)create: segments, text (from the indices of the selected utterances), then spk2utt, utt2spk 
	# 4)copy spk2utt utt2spk wav.scp text segments reco2file_and_channel to data/train
	# 5)fix data dir: ./utils/fix_data_dir.sh data/train/
	
	mkdir -p data/local/train/
	mkdir -p data/train/
	mkdir -p data/local/data
	# you have to create the physic link to the Fisher dataset directory
	# please see fisher_english/s5/local/fisher_data_prep.sh in the original Kaldi release to prepare the right file
	ln -sf /s1/yzliu/asr/recipe/fisher_english/s5/data/local/data/links data/local/data/links;
	
	uttrid=lists/out_1000_Utt.list
	wordid=lists/out_1000_Vocab.list
	
	cp fisher-data/reco2file_and_channel data/local/train
	cp fisher-data/wav.scp data/local/train
	
	cp $uttrid data/local/train/uttr.id
	cp $wordid data/local/train/word.id
	
	python helper/get-groundset.py -d data/local/train/uttr.id -i fisher-data/segments -o data/local/train/segments
	python helper/get-groundset.py -d data/local/train/uttr.id -i fisher-data/text -o data/local/train/text
	python helper/get-groundset.py -d data/local/train/uttr.id -i fisher-data/utt2spk -o data/local/train/utt2spk
	
	./utils/utt2spk_to_spk2utt.pl data/local/train/utt2spk > data/local/train/spk2utt
	
	cp data/local/train/{segments,text,wav.scp,utt2spk,spk2utt,reco2file_and_channel} data/train
	
	./utils/fix_data_dir.sh data/train		

	# prepare LM and dictionary
	helper/fivher_prepare_dict.sh;
	utils/prepare_lang.sh data/local/dict '!sil' data/local/lang data/lang
fi

#############################################
# Stage 2
#############################################
if [ $stage = 2 ]; then	
	mfccdir=mfcc
	steps/make_mfcc.sh --compress true --nj 20 --cmd "$train_cmd" data/train exp/make_mfcc/train $mfccdir
	steps/compute_cmvn_stats.sh data/train exp/make_mfcc/train $mfccdir 
	utils/fix_data_dir.sh data/train
fi


#############################################
# Stage 3
#############################################
# split into train/dev/eval
if [ $stage = 3 ]; then

	utils/subset_data_dir_tr_cv.sh --cv-utt-percent 2 data/train data/tune data/train_dev_eval;
	utils/subset_data_dir_tr_cv.sh --cv-utt-percent 50 data/train_dev_eval data/dev data/eval;

	rm -rf data/train_dev_eval;
	
	echo "Prepare LM"
	helper/fivher_train_lms.sh data/tune/text data/local/dict/lexicon.txt data/local/lm || exit 1;

	srilm_opts="-subset -prune -lowprobs  -tolower -order 3"
	LM=data/local/lm/sw1.o3g.kn.gz

	utils/format_lm_sri.sh --srilm-opts "$srilm_opts" \
	  data/lang $LM data/local/dict/lexicon.txt data/lang_fsh_tg || exit 1;		
	
fi


#############################################
# Stage 4
#############################################
if [ $stage = 4 ]; then
	steps/train_mono.sh --nj 20 --cmd "$train_cmd" \
	  data/tune data/lang exp/mono

	steps/align_si.sh --nj 20 --cmd "$train_cmd" \
	  data/tune data/lang exp/mono exp/mono_ali

	steps/train_deltas.sh --cmd "$train_cmd" \
	  2500 25000 data/tune data/lang exp/mono_ali exp/tri1

	for lm_suffix in tg; do
	    graph_dir=exp/tri1/graph_sw1_${lm_suffix}
	    $train_cmd $graph_dir/mkgraph.log \
	      utils/mkgraph.sh data/lang_fsh_${lm_suffix} exp/tri1 $graph_dir
	    steps/decode_si.sh --nj 20 --cmd "$decode_cmd" --config conf/decode.config \
	      $graph_dir data/dev exp/tri1/decode_dev
	    steps/decode_si.sh --nj 20 --cmd "$decode_cmd" --config conf/decode.config \
	      $graph_dir data/eval exp/tri1/decode_eval
	done

fi


#############################################
# Stage DNN
#############################################
if [ $stage = 5 ]; then
	batchsize=256
	
	steps/align_si.sh --nj 20 --cmd "$train_cmd" \
	  data/tune data/lang exp/tri1 exp/tri1_ali
		
	./steps/nnet2/train_tanh_gpu.sh --initial-learning-rate 0.01 --final-learning-rate 0.001 --samples-per-iter 400000 --num-hidden-layers 4 --hidden-layer-dim 1024 --minibatch-size $batchsize data/tune data/lang exp/tri1_ali exp/tri1_nnet

	./utils/mkgraph.sh data/lang_fsh_tg exp/tri1_nnet exp/tri1_nnet/graph

	./steps/nnet2/decode.sh --nj 20 exp/tri1_nnet/graph data/dev exp/tri1_nnet/decode_dev;
	./steps/nnet2/decode.sh --nj 20 exp/tri1_nnet/graph data/eval exp/tri1_nnet/decode_eval;
fi

