import sys,os
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-i", action="store", type="string", dest="input")
parser.add_option("-d", action="store", type="string", dest="ids")
parser.add_option("-o", action="store", type="string", dest="output")


(options,args) = parser.parse_args()

def main():
    lines=open(options.ids).readlines()
    ids=set()
    for i in lines:
        ids.add(i.split()[0])        
    
    
    fi=open(options.input,'r')
    fo=open(options.output,'w')
    while 1:
        line=fi.readline()
        if len(line)==0:
            break
        else:
            if line.split()[0] in ids:
                fo.write(line)
                
                
if __name__=='__main__':
    main()
