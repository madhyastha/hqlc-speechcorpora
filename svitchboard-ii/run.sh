#!/bin/bash

# A walkthrough of SVB ASR system

. cmd.sh
. path.sh
set -e # exit on error

stage=$1

if [ -z $1 ]; then
	echo "Specify stage";
	exit;
fi
#############################################
# Stage 1
#############################################
if [ $stage = 1 ]; then
	# in the following directory: data/local/train/
	# 1)copy: reco2file_and_channel wav.scp
	# 2)create:  uttr.id    word.id
	# 3)create: segments, text (from the indices of the selected utterances), then spk2utt, utt2spk 
	# 4)copy spk2utt utt2spk wav.scp text segments reco2file_and_channel to data/train
	# 5)fix data dir: ./utils/fix_data_dir.sh data/train/
	
	mkdir -p data/local/train/
	mkdir -p data/train/

	# python helper/extract-uttr.py -i lists/out_5003_Vocab.txt -s swbd-data/words.txt -o lists/out_5003_Vocab.list
	# python helper/extract-uttr.py -i lists/out_5003_Utt.txt -s swbd-data/swbI.transcripts.text -o lists/out_5003_Utt.list
	uttrid=lists/out_5003_Utt.list
	wordid=lists/out_5003_Vocab.list
	
	cp swbd-data/reco2file_and_channel data/local/train
	cp swbd-data/wav.scp data/local/train
	
	cp $uttrid data/local/train/uttr.id
	cp $wordid data/local/train/word.id
	
	python helper/get-groundset.py -d data/local/train/uttr.id -i swbd-data/segments -o data/local/train/segments
	python helper/get-groundset.py -d data/local/train/uttr.id -i swbd-data/text -o data/local/train/text
	python helper/get-groundset.py -d data/local/train/uttr.id -i swbd-data/utt2spk -o data/local/train/utt2spk
	
	./utils/utt2spk_to_spk2utt.pl data/local/train/utt2spk > data/local/train/spk2utt
	
	cp data/local/train/{segments,text,wav.scp,utt2spk,spk2utt,reco2file_and_channel} data/train
	
	./utils/fix_data_dir.sh data/train		

	# prepare LM and dictionary
	local/swbd1_prepare_dict2.sh
	utils/prepare_lang.sh data/local/dict '!sil'  data/local/lang data/lang
fi


#############################################
# Stage 2
#############################################
if [ $stage = 2 ]; then	
	mfccdir=mfcc
	steps/make_mfcc.sh --compress true --nj 20 --cmd "$train_cmd" data/train exp/make_mfcc/train $mfccdir
	steps/compute_cmvn_stats.sh data/train exp/make_mfcc/train $mfccdir 
	utils/fix_data_dir.sh data/train
fi

#############################################
# Stage 3
#############################################
# split into train/dev/eval, we split the data into 5 folds (A/B/C/D/E). 
# For experiments, we use 4 of the 5 folds as training data (makred in data/tune*), the first half of the remaining fold as dev data (marked in data/dev*), and the second half of the remaining fold as evaluation data (marked in data/eval*)
# This is a suggested data partitioning scheme. However, it is up to the users to decide their preferred split.
if [ $stage = 3 ]; then

	utils/subset_data_dir_tr_cv.sh --cv-utt-percent 20 data/train data/train_tr data/train_cv1;
	utils/subset_data_dir_tr_cv.sh --cv-utt-percent 25 data/train_tr data/train_tr2 data/train_cv2;
	utils/subset_data_dir_tr_cv.sh --cv-utt-percent 33 data/train_tr2 data/train_tr3 data/train_cv3;
	utils/subset_data_dir_tr_cv.sh --cv-utt-percent 50 data/train_tr3 data/train_cv5 data/train_cv4;
	# utils/subset_data_dir_tr_cv.sh --cv-utt-percent 50 data/train_tr4 data/train_cv6 data/train_cv5;
	rm -rf data/train_tr*;

	for ((i=1;i<=5;i++))
	do
		utils/subset_data_dir_tr_cv.sh --cv-utt-percent 50 data/train_cv${i} data/train_cv${i}_dev data/train_cv${i}_eval
		awk '{print $1}' data/train_cv${i}/spk2utt > data/train/spklist.split${i};		
	done

	# ABC/D/E
	echo "CV-1"
	cat data/train/spklist.split{1,2,3,4} > data/train/tune1.spk.list
	cat data/train_cv5_dev/spk2utt > data/train/dev1.spk.list
	cat data/train_cv5_eval/spk2utt > data/train/eval1.spk.list

	utils/subset_data_dir.sh --spk-list data/train/tune1.spk.list data/train data/tune1
	utils/subset_data_dir.sh --spk-list data/train/dev1.spk.list data/train data/dev1
	utils/subset_data_dir.sh --spk-list data/train/eval1.spk.list data/train data/eval1

	# BCD/E/A
	echo "CV-2"
	cat data/train/spklist.split{2,3,4,5} > data/train/tune2.spk.list
	cat data/train_cv1_dev/spk2utt > data/train/dev2.spk.list
	cat data/train_cv1_eval/spk2utt > data/train/eval2.spk.list
	
	utils/subset_data_dir.sh --spk-list data/train/tune2.spk.list data/train data/tune2
	utils/subset_data_dir.sh --spk-list data/train/dev2.spk.list data/train data/dev2
	utils/subset_data_dir.sh --spk-list data/train/eval2.spk.list data/train data/eval2

	# CDE/A/B
	echo "CV-3"
	cat data/train/spklist.split{3,4,5,1} > data/train/tune3.spk.list
	cat data/train_cv2_dev/spk2utt > data/train/dev3.spk.list
	cat data/train_cv2_eval/spk2utt > data/train/eval3.spk.list
	
	utils/subset_data_dir.sh --spk-list data/train/tune3.spk.list data/train data/tune3
	utils/subset_data_dir.sh --spk-list data/train/dev3.spk.list data/train data/dev3
	utils/subset_data_dir.sh --spk-list data/train/eval3.spk.list data/train data/eval3

	# DEA/B/C
	echo "CV-4"
	cat data/train/spklist.split{4,5,1,2} > data/train/tune4.spk.list
	cat data/train_cv3_dev/spk2utt > data/train/dev4.spk.list
	cat data/train_cv3_eval/spk2utt > data/train/eval4.spk.list
	
	utils/subset_data_dir.sh --spk-list data/train/tune4.spk.list data/train data/tune4
	utils/subset_data_dir.sh --spk-list data/train/dev4.spk.list data/train data/dev4
	utils/subset_data_dir.sh --spk-list data/train/eval4.spk.list data/train data/eval4

	# EAB/C/D
	echo "CV-5"
	cat data/train/spklist.split{5,1,2,3} > data/train/tune5.spk.list
	cat data/train_cv4_dev/spk2utt > data/train/dev5.spk.list
	cat data/train_cv4_eval/spk2utt > data/train/eval5.spk.list
	
	utils/subset_data_dir.sh --spk-list data/train/tune5.spk.list data/train data/tune5
	utils/subset_data_dir.sh --spk-list data/train/dev5.spk.list data/train data/dev5
	utils/subset_data_dir.sh --spk-list data/train/eval5.spk.list data/train data/eval5

	echo "Prepare LM"
	for ((cv=1;cv<=5;cv++))
	do

		echo "CV fold $cv"
		echo "Prepare LM"
		local/swbd1_train_lms_nofisher.sh data/tune${cv}/text data/local/dict/lexicon.txt data/local/lm${cv} || exit 1;

		srilm_opts="-subset -prune -lowprobs  -tolower -order 3"
		LM=data/local/lm${cv}/sw1.o3g.kn.gz

		utils/format_lm_sri.sh --srilm-opts "$srilm_opts" \
		  data/lang $LM data/local/dict/lexicon.txt data/lang_sw1_tg${cv} || exit 1;

	done

fi

#############################################
# Stage 4: GMM-HMM
#############################################
# This is a simple context-dependent triphone GMM-HMM system.
# An initial monophone system is trained, followed by the triphone system training.
# The features in the system are simple MFCC+delta+delta/delta with per-speaker mean normalization.

if [ $stage = 4 ]; then
	for ((cv=1;cv<=5;cv++))
	do
		echo "CV fold $cv"	
		steps/train_mono.sh --nj 20 --cmd "$train_cmd" \
		  data/tune${cv} data/lang exp/mono_cv${cv} 

		steps/align_si.sh --nj 20 --cmd "$train_cmd" \
		  data/tune${cv} data/lang exp/mono_cv${cv} exp/mono_cv${cv}_ali 

		steps/train_deltas.sh --cmd "$train_cmd" \
		  2500 25000 data/tune${cv} data/lang exp/mono_cv${cv}_ali exp/tri1_cv${cv} 

		for lm_suffix in tg; do
		    graph_dir=exp/tri1_cv${cv}/graph_sw1_${lm_suffix}
		    $train_cmd $graph_dir/mkgraph.log \
		      utils/mkgraph.sh data/lang_sw1_${lm_suffix}${cv} exp/tri1_cv${cv} $graph_dir
		    steps/decode_si.sh --nj 20 --cmd "$decode_cmd" --config conf/decode.config \
		      $graph_dir data/dev${cv} exp/tri1_cv${cv}/decode_dev${cv}
		    steps/decode_si.sh --nj 20 --cmd "$decode_cmd" --config conf/decode.config \
		      $graph_dir data/eval${cv} exp/tri1_cv${cv}/decode_eval${cv}	
		done
	done
fi

#############################################
# Stage 5: DNN-HMM
#############################################
# This is a simple DNN-HMM system on top of the previous GMM-based system.

if [ $stage = 5 ]; then
	batchsize=1024
	
	for ((cv=1;cv<=5;cv++))
	do
		echo "CV fold $cv"	
		steps/align_si.sh --nj 20 --cmd "$train_cmd" \
		  data/tune${cv} data/lang exp/tri1_cv${cv} exp/tri1_cv${cv}_ali 
		
		
		./steps/nnet2/train_tanh_gpu.sh --initial-learning-rate 0.01 --final-learning-rate 0.001 --samples-per-iter 400000 --num-hidden-layers 4 --hidden-layer-dim 1024 --minibatch-size $batchsize data/tune${cv} data/lang exp/tri1_cv${cv}_ali exp/tri1_cv${cv}_nnet

		./utils/mkgraph.sh data/lang_sw1_tg${cv} exp/tri1_cv${cv}_nnet exp/tri1_cv${cv}_nnet/graph

		./steps/nnet2/decode.sh --nj 20 exp/tri1_cv${cv}_nnet/graph data/dev${cv} exp/tri1_cv${cv}_nnet/decode_dev${cv};
		./steps/nnet2/decode.sh --nj 20 exp/tri1_cv${cv}_nnet/graph data/eval${cv} exp/tri1_cv${cv}_nnet/decode_eval${cv};
	done
fi

