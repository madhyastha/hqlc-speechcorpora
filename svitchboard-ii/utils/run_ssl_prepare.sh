#!/bin/bash

# this script prepares the LM/ dict/ lexicon related stuff

. cmd.sh
. path.sh
set -e # exit on error

# define labeled and unlabeled set (the spk-list is computed before run this)
utils/subset_data_dir.sh --spk-list /s1/yzliu/spk.lab.list data/train_100k_nodup  data/ssl/train_100k_seed 
utils/subset_data_dir.sh --spk-list /s1/yzliu/spk.unl.list data/train_100k_nodup  data/ssl/train_100k_unlabeled

local/swbd1_train_lms.sh data/ssl/train_100k_seed/text data/local/dict/lexicon.txt data/ssl/lm

# We don't really need all these options for SRILM, since the LM training script
# does some of the same processings (e.g. -subset -tolower)
srilm_opts="-subset -prune-lowprobs -unk -tolower -order 3"
LM=data/ssl/lm/sw1.o3g.kn.gz
utils/format_lm_sri.sh --srilm-opts "$srilm_opts" \
  data/lang $LM data/local/dict/lexicon.txt data/ssl/lang_sw1_tg

utils/subset_data_dir.sh data/ssl/train_100k_seed  3000 data/ssl/train_3k
