#!/bin/bash

# This recipe is based on the run_edin.sh recipe, by Arnab Ghoshal,
# in the s5/ directory.
# This is supposed to be the "new" version of the switchboard recipe,
# after the s5/ one became a bit messy.  It is not 100% checked-through yet.

#exit 1;
# This is a shell script, but it's recommended that you run the commands one by
# one by copying and pasting into the shell.
# Caution: some of the graph creation steps use quite a bit of memory, so you
# should run this on a machine that has sufficient memory.

. cmd.sh
. path.sh
set -e # exit on error

# Train tri3b_new, which is LDA+MLLT, on 100k_nodup data.
steps/train_lda_mllt.sh --cmd "$train_cmd" \
  3200 30000 data/train_100k_nodup data/lang exp/tri2_ali_100k_nodup exp/tri3b_new

for lm_suffix in tg; do
  (
    graph_dir=exp/tri3b_new/graph_sw1_${lm_suffix}
    $train_cmd $graph_dir/mkgraph.log \
      utils/mkgraph.sh data/lang_sw1_${lm_suffix} exp/tri3b_new $graph_dir
    steps/decode.sh --nj 30 --cmd "$decode_cmd" --config conf/decode.config \
      $graph_dir data/eval2000 exp/tri3b_new/decode_eval2000_sw1_${lm_suffix}
  ) &
done

# Train tri4a_new, which is LDA+MLLT+SAT, on 100k_nodup data.
steps/align_fmllr.sh --nj 30 --cmd "$train_cmd" \
  data/train_100k_nodup data/lang exp/tri3b_new exp/tri3b_new_ali_100k_nodup


steps/train_sat.sh  --cmd "$train_cmd" \
  3200 30000 data/train_100k_nodup data/lang exp/tri3b_new_ali_100k_nodup \
   exp/tri4a_new

for lm_suffix in tg; do
  (
    graph_dir=exp/tri4a_new/graph_sw1_${lm_suffix}
    $train_cmd $graph_dir/mkgraph.log \
      utils/mkgraph.sh data/lang_sw1_${lm_suffix} exp/tri4a_new $graph_dir
    steps/decode_fmllr.sh --nj 30 --cmd "$decode_cmd" --config conf/decode.config \
      $graph_dir data/eval2000 exp/tri4a_new/decode_eval2000_sw1_${lm_suffix}
  ) &
done


steps/align_fmllr.sh --nj 50 --cmd "$train_cmd" \
  data/train_100k_nodup data/lang exp/tri4a_new exp/tri4a_new_ali_100k_nodup || exit 1

