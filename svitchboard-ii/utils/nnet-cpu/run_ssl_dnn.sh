#!/bin/bash

. cmd.sh
. path.sh
set -e # exit on error

./steps/nnet2/train_tanh.sh \
	--initial-learning-rate 0.01 --final-learning-rate 0.001 --samples-per-iter 400000 \
	--num-hidden-layers 4 --hidden-layer-dim 1200 \
	data/ssl/train_100k_seed data/lang exp_ssl/tri4a_ali exp_ssl/tri4a_nnet

utils/mkgraph.sh data/ssl/lang_sw1_tg exp_ssl/tri4a_nnet exp_ssl/tri4a_nnet/graph_sw1_tg

./steps/nnet2/decode.sh --transform-dir exp_ssl/tri4a/decode_eval2000_sw1_tg --config conf/decode.config --nj 20 exp_ssl/tri4a_nnet/graph_sw1_tg data/eval2000 exp_ssl/tri4a_nnet/decode_eval2000_sw1_tg